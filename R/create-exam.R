#' Render a .pdf of exam questions based on input questions and config in .json files
#'
#' @param question.file path to .json file containing questions (character)
#' @param config.file path to .json file containing configuration (character)
#' @param exam.name name of the exam used for labeling the output files; extensions will be stripped (character)
#' @param custom.tex (UNUSED) path to file containing custom tex configuration (character)
#' @param engine tex engine you want to use to render the .pdf (character)
#' @param form.number indicator for the form (character)
#' @param output.directory (UNUSED) output directory of the exam (character)
#' @export
#'
create_exam <- function(question.file, config.file, exam.name, engine = "pdflatex", custom.tex = NULL, form.number = NULL, output.directory = NULL) {

  ## TODO make a default header as a dataset that can be referenced in here somehow
  ## create name of output file
  tex.filename <- create_name(exam.name)

  ## if form number is present, strip .tex extension and put in form number after exam name
  if (!is.null(form.number)) {
    tex.filename <- tex.filename %>%
      stringr::str_split(., "\\.") %>%
      purrr::map_chr(1) %>%
      paste0(., "-", form.number) %>%
      create_name()
  }

  ## knit the exam, create .tex, retrieve key
  key <- knit_exam(question.file, config.file, out.file = tex.filename, form.number)

  ## create name for answer key
  csv.filename <- tex.filename  %>%
    stringr::str_split(".tex") %>%
    purrr::map_chr(1) %>%
    paste0(., "-key.csv")

  ## save the answer key (probably not the best place to do this)
  readr::write_csv(key, csv.filename)

  ## create .pdf from .tex
  render_pdf(tex.filename, engine)
}

#' Wrapper around the function create_exam
#'
#' @param forms number of forms you wish to create (integer)
#' @inheritParams create_exam
#' @export
#'
create_exams <- function(question.file, config.file, exam.name, forms, engine = "pdflatex", custom.tex = NULL) {

  ## create capital letters of forms
  form.numbers <- letters[1:forms] %>%
    toupper()

  ## iterate over forms
  for (i in form.numbers) {
    create_exam(question.file, config.file, exam.name, engine, custom.tex, form.number = i)
    }
}
